package demo.service;

import com.fasterxml.jackson.databind.JsonNode;
import demo.config.model.AppDefinition;
import demo.config.model.MappingDefinition;
import demo.config.RequestMappingConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PUT;

/**
 *
 * @author boonyasukd
 */
@Service
public class RequestProcessor implements RestSupport {

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private RequestMappingConfiguration config;

    @Autowired
    private RestTemplate restTemplate;

    public JsonNode processRequest(String urlPattern, HttpMethod method, Map<String, String> params) {
        return processRequest(urlPattern, method, params, null);
    }

    public JsonNode processRequest(String urlPattern, HttpMethod method, Map<String, String> params, JsonNode body) {
        MappingDefinition mapping = config.findMappingDefinition(method, urlPattern);
        String url = mapping.getOutboundAbsoluteUrl();
        JsonNode response = request(method, url, params, body);

        return (method == PUT || method == DELETE) ? null : transformResponse(response, mapping, params);
    }

    public JsonNode transformResponse(JsonNode sourceResponse, MappingDefinition mapping, Map<String, String> params) {
        try {
            AppDefinition app = mapping.getParent();
            Object bean = ctx.getBean(app.getTransformBean());
            Method beanMethod = bean.getClass().getDeclaredMethod(mapping.getTransformMethod(), JsonNode.class, Map.class, AppDefinition.class);

            params.put("pathPrefix", config.getSinghaUrlPrefix() + "/" + app.getName());
            return (JsonNode) beanMethod.invoke(bean, sourceResponse, params, app);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }
}
