package demo.service.proxy.odigos;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import demo.config.model.AppDefinition;
import demo.service.proxy.JsonTransformer;
import java.util.List;
import org.springframework.stereotype.Service;

import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 *
 * @author boonyasukd
 */
@Service
public class OdigosProxyService extends JsonTransformer {

    public JsonNode transformPubList(JsonNode input, Map<String, String> params, AppDefinition appDef) {
        List<ObjectNode> publications = toStream(((ObjectNode) input).get("publicationMap").fields())
                .map(entry -> {
                    params.put("wwwDomain", entry.getValue().asText());

                    return newObjectNode()
                        .put("name", entry.getValue().asText())
                        .put("id", entry.getKey())
                        .putPOJO("_links", appDef.getLinks(params, "update redirect rules"));
                }).collect(toList());

        return newObjectNode().putPOJO("publications", publications);
    }

    public JsonNode transformRuleset(JsonNode input, Map<String, String> params, AppDefinition appDef) {
        return ((ObjectNode) input).putPOJO("_links", appDef.getLinks(params, "view publications"));
    }
}
