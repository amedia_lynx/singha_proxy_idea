package demo.service.proxy.meetix;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import demo.config.model.AppDefinition;
import demo.service.proxy.JsonTransformer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static demo.service.proxy.meetix.MeetixJsonTransformer.toMeeting;
import static demo.service.proxy.meetix.MeetixJsonTransformer.toPublication;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author boonyasukd
 */
@Service
public class MeetixProxyService extends JsonTransformer {

    public JsonNode transformPubList(JsonNode input, Map<String, String> params, AppDefinition appDef) {
        List<ObjectNode> publications = toStream(((ObjectNode) input).get("publicationMap").fields())
                .map(toPublication(params, appDef))
                .collect(toList());

        return newObjectNode().putPOJO("publications", publications);
    }

    public JsonNode transformNetmeetingList(JsonNode input, Map<String, String> params, AppDefinition appDef) {
        List<ObjectNode> netmeetings = toStream(input).map(toMeeting(params, appDef)).collect(toList());

        return newObjectNode()
                .putPOJO("netmeetings", netmeetings)
                .putPOJO("_links", appDef.getLinks(params, "create netmeeting"));
    }
}
