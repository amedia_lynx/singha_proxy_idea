package demo.service.proxy.meetix;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import demo.config.model.AppDefinition;
import demo.service.proxy.JsonTransformer;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import static java.util.Arrays.asList;

public class MeetixJsonTransformer extends JsonTransformer {
    private static final String[] PUB_ACTIONS = {"show netmeeting list", "show person list"};
    private static final String[] MEETING_ACTIONS = {"update netmeeting", "delete netmeeting", "create new person"};

    protected static Function<Entry<String, JsonNode>, ObjectNode> toPublication(Map<String, String> params, AppDefinition appDef) {
        return entry -> {
            return newObjectNode()
                    .put("name", entry.getValue().asText())
                    .put("id", entry.getKey())
                    .putPOJO("_links", appDef.getLinks(params, PUB_ACTIONS));
        };
    }

    protected static Function<JsonNode, ObjectNode> toMeeting(Map<String, String> params, AppDefinition appDef) {
        return netMeeting -> {
            params.put("netmeetingId", netMeeting.get("id").asText());

            return ((ObjectNode) netMeeting)
                    .putPOJO("_links", appDef.getLinks(params, MEETING_ACTIONS));
        };
    }

    protected static Function<JsonNode, ObjectNode> toPerson(Map<String, String> params, AppDefinition appDef) {
        return jsonNode -> {
            params.put("personId", jsonNode.get("id").asText());
            params.put("netmeetingId", jsonNode.get("netmeetingId").asText());

            return ((ObjectNode) jsonNode)
                    .remove(asList("fullName"))
                    .putPOJO("_links", appDef.getLinks(params, "delete person"));
        };
    }

    protected static Function<JsonNode, ObjectNode> toPerson2(Map<String, String> params, AppDefinition appDef) {
        return jsonNode -> {
            params.put("personId", jsonNode.get("id").asText());

            return ((ObjectNode) jsonNode).putPOJO("_links", appDef.getLinks(params, "delete person"));
        };
    }

    protected static Function<JsonNode, ObjectNode> toQuestion(Map<String, String> params, AppDefinition appDef) {
        return jsonNode -> {
            params.put("questionId", jsonNode.get("id").asText());

            return ((ObjectNode) jsonNode).putPOJO("_links", appDef.getLinks(params, "delete netmeeting"));
        };
    }
}
