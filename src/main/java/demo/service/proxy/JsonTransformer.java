package demo.service.proxy;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;

import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 *
 * @author boonyasukd
 */
public class JsonTransformer {

    protected static ObjectNode newObjectNode() {
        return JsonNodeFactory.instance.objectNode();
    }

    protected static Map<String, String> linkObj(String title, String href, Enum<?> method) {
        return linkObj(title, href, method, null);
    }

    protected static Map<String, String> linkObj(String title, String href, Enum<?> method, String schema) {
        if (isNullOrEmpty(schema)) {
            return ImmutableMap.of("title", title, "href", href, "method", method.name());
        } else {
            return ImmutableMap.of("title", title, "href", href, "method", method.name(), "schema", schema);
        }
    }

    protected static <T> Stream<T> toStream(Iterator<T> iter) {
        return toStream(() -> iter);
    }

    protected static <T> Stream<T> toStream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
