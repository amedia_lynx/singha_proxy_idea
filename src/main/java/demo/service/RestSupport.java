package demo.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestClientException;

import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.ACCEPT_CHARSET;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 *
 * @author boonyasukd
 */
public interface RestSupport {
    static final Logger log = LoggerFactory.getLogger(RestSupport.class);

    RestTemplate getRestTemplate();

    default JsonNode request(HttpMethod method, String url, Map<String, String> params) {
        return request(method, url, params, buildEntity(method));
    }

    default JsonNode request(HttpMethod method, String url, Map<String, String> params, JsonNode body) {
        return request(method, url, params, buildEntity(method, body));
    }

    default JsonNode request(HttpMethod method, String url, Map<String, String> params, HttpEntity entity) {
        try {
            switch (method) {
                case PUT:
                case DELETE:
                    getRestTemplate().exchange(url, method, entity, (Class<?>) null, params);
                    return null;
                default:
                    return getRestTemplate().exchange(url, method, entity, JsonNode.class, params).getBody();
            }
        } catch (RestClientException ex) {
            log.error("Error while making HTTP request: ({}) {} with params {}", method, url, params);
            throw new RuntimeException(ex);
        }
    }

    default HttpEntity buildEntity(HttpMethod method) {
        return buildEntity(method, null);
    }

    default HttpEntity buildEntity(HttpMethod method, JsonNode body) {
        HttpHeaders headers = new HttpHeaders();

        if (method == POST || method == PUT) {
            headers.add(CONTENT_TYPE, APPLICATION_JSON.toString() + ";charset=utf-8");
        }

        if (method == GET || method == POST) {
            headers.add(ACCEPT, APPLICATION_JSON.toString());
            headers.add(ACCEPT_CHARSET, "utf-8");
        }

        return new HttpEntity(body, headers);
    }
}
