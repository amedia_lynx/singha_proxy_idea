package demo.service;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

/**
 *
 * @author boonyasukd
 */
public class SinghaRestErrorHandler implements ResponseErrorHandler {
    private static final Logger log = LoggerFactory.getLogger(SinghaRestErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse chr) throws IOException {
        HttpStatus.Series series = chr.getStatusCode().series();
        return series == CLIENT_ERROR || series == SERVER_ERROR;
    }

    @Override
    public void handleError(ClientHttpResponse chr) throws IOException {
        log.error("Error requesting HTTP request with error code {} and message '{}'", chr.getRawStatusCode(), chr.getStatusText());
        throw new RestClientException("Error requesting HTTP request");
    }
}
