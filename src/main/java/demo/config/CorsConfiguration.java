package demo.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

import static java.util.stream.Collectors.joining;

/**
 *
 * @author boonyasukd
 */
@Configuration
@ConfigurationProperties(prefix = "config.cors")
public class CorsConfiguration {

    private final List<String> origins = new ArrayList<>();
    private final List<String> methods = new ArrayList<>();
    private Boolean allowCredentials;
    private Integer maxAge;

    public List<String> getOrigins() {
        return origins;
    }

    public List<String> getMethods() {
        return methods;
    }

    public Boolean getAllowCredentials() {
        return allowCredentials;
    }

    public void setAllowCredentials(Boolean allowCredentials) {
        this.allowCredentials = allowCredentials;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        //headers.add("Access-Control-Allow-Origin", origins.stream().collect(joining(", ")));
        headers.add("Access-Control-Allow-Methods", methods.stream().collect(joining(", ")));
        headers.add("Access-Control-Allow-Credentials", getAllowCredentials().toString());
        headers.add("Access-Control-Max-Age", getMaxAge().toString());

        return headers;
    }
}
