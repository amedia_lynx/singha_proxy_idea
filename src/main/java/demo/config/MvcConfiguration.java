package demo.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author boonyasukd
 */
@Configuration
@ConfigurationProperties(prefix = "config.mvc")
public class MvcConfiguration {
    private static final Logger log = LoggerFactory.getLogger(MvcConfiguration.class);

    @Autowired
    private CorsConfiguration cors;

    @Autowired
    private RequestMappingConfiguration mappingConfig;

    private final Map<String, List<String>> interceptors = new HashMap<>();

    private final List<String> interceptorExcludePaths = new ArrayList<>();

    @PostConstruct
    private void init() {

    }

    @PreDestroy
    private void destroy() {

    }

    public Map<String, List<String>> getInterceptors() {
        return interceptors;
    }

    public List<String> getInterceptorExcludePaths() {
        return interceptorExcludePaths;
    }

    public WebMvcConfigurerAdapter getMvcConfigurer() {
        return new WebMvcConfigurerAdapter() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins(cors.getOrigins().toArray(new String[0]))
                        .allowedMethods(cors.getMethods().toArray(new String[0]))
                        .allowCredentials(cors.getAllowCredentials())
                        .maxAge(cors.getMaxAge());
            }

            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                mappingConfig.getApps().values().stream().forEach(app -> {
                    app.getStaticFileMappings().forEach((requestPath, fileLocation) -> {
                        registry.addResourceHandler(requestPath)
                                .addResourceLocations(fileLocation);
//                                .setCachePeriod(3600);
                    });
                });
            }
        };
    }
}
