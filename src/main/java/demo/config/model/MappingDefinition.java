package demo.config.model;

import demo.service.proxy.JsonTransformer;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.util.DefaultUriTemplateHandler;

/**
 *
 * @author boonyasukd
 */
public class MappingDefinition extends JsonTransformer {
    private AppDefinition parent;
    private String url;
    private RequestMethod method;
    private String[] params;
    private String schemaName;
    private String actionText;
    private String breadcrumbText;
    private String transformMethod;
    private boolean rootNode = false;

    // inbound/outbound url templates
    private String inboundRelativeUrl;
    private String inboundAbsoluteUrl;
    private String outboundAbsoluteUrl;

    public AppDefinition getParent() {
        return parent;
    }

    public void setParent(AppDefinition parent) {
        this.parent = parent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public void setMethod(RequestMethod method) {
        this.method = method;
    }

    public String[] getParams() {
        return params;
    }

    public void setParams(String[] params) {
        this.params = params;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    public String getBreadcrumbText() {
        return breadcrumbText;
    }

    public void setBreadcrumbText(String breadcrumbText) {
        this.breadcrumbText = breadcrumbText;
    }

    public boolean isRootNode() {
        return rootNode;
    }

    public void setRootNode(boolean rootNode) {
        this.rootNode = rootNode;
    }

    public String getTransformMethod() {
        return transformMethod;
    }

    public void setTransformMethod(String transformMethod) {
        this.transformMethod = transformMethod;
    }

    public String getInboundRelativeUrl() {
        return inboundRelativeUrl;
    }

    public String getInboundAbsoluteUrl() {
        return inboundAbsoluteUrl;
    }

    public String getOutboundAbsoluteUrl() {
        return outboundAbsoluteUrl;
    }

    public void initUrlTemplates(String singhaUrlPrefix, String singhaPathPrefix, String serviceUrlPrefix) {
        inboundRelativeUrl = singhaPathPrefix + "/" + parent.getName() + url;
        inboundAbsoluteUrl = singhaUrlPrefix + "/" + parent.getName() + url + "?token={token}";
        outboundAbsoluteUrl = serviceUrlPrefix + parent.getPathPrefix() + url + "?token={token}";
    }

    public boolean matches(RequestMethod method, String input) {
        return this.method == method && this.inboundRelativeUrl.equals(input);
    }

    public boolean actionNameMatches(String actionText) {
        return this.actionText.equals(actionText);
    }

    public RequestMappingInfo asRequestMappingInfo(String prefix) {
        return RequestMappingInfo
                .paths(inboundRelativeUrl)
                .methods(method)
                .params(params)
                .build();
    }

    public Map<String, String> asLinkObj(Map<String, String> params) {
        String linkUrl = new DefaultUriTemplateHandler().expand(inboundAbsoluteUrl, params).toString();
        return linkObj(actionText, linkUrl, method, schemaName);
    }

    @Override
    public String toString() {
        return "MappingDefinition{" + " url=" + url + ", method=" + method + ", params=" + params + ", schemaName=" + schemaName + ", actionText=" + actionText + ", breadcrumbText=" + breadcrumbText + ", transformMethod=" + transformMethod + ", rootNode=" + rootNode + ", \ninboundRelativeUrl=" + inboundRelativeUrl + ", \ninboundAbsoluteUrl=" + inboundAbsoluteUrl + ", \noutboundAbsoluteUrl=" + outboundAbsoluteUrl + '}';
    }
}
