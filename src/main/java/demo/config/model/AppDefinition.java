package demo.config.model;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import demo.service.proxy.JsonTransformer;

/**
 *
 * @author boonyasukd
 */
public class AppDefinition extends JsonTransformer {
    private String name;
    private String urlPrefixProperty;
    private String pathPrefix;
    private String transformBean;
    private List<String> schemas;
    private final List<MappingDefinition> mappings = new ArrayList<>();

    public void initSchemaList() {
        try {
            String scanPath = "classpath*:config/apps/" + name + "/*.json";
            schemas = Arrays.stream(new PathMatchingResourcePatternResolver().getResources(scanPath))
                    .map(Resource::getFilename)
                    .map(str -> str.substring(0, str.indexOf(".")))
                    .sorted()
                    .collect(toList());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPrefixProperty() {
        return urlPrefixProperty;
    }

    public void setUrlPrefixProperty(String urlPrefixProperty) {
        this.urlPrefixProperty = urlPrefixProperty;
    }

    public String getPathPrefix() {
        return pathPrefix;
    }

    public void setPathPrefix(String pathPrefix) {
        this.pathPrefix = pathPrefix;
    }

    public String getTransformBean() {
        return transformBean;
    }

    public void setTransformBean(String transformBean) {
        this.transformBean = transformBean;
    }

    public List<MappingDefinition> getMappings() {
        return mappings;
    }

    public List<String> getSchemas() {
        return schemas;
    }

    public JsonNode getLinks(Map<String, String> params, String... actionTexts) {
        ObjectNode result = newObjectNode();
        Arrays.stream(actionTexts).forEach(actionText -> {
            result.putPOJO(actionText, getLinkObj(actionText, params));
        });

        return result;
    }

    private Map<String, String> getLinkObj(String actionText, Map<String, String> params) {
        return mappings.stream()
                .filter(mapping -> mapping.actionNameMatches(actionText))
                .map(mapping -> mapping.asLinkObj(params))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("mapping by actionText '" + actionText + "' not found."));
    }

    public Map<String, String> getStaticFileMappings() {
        return singletonMap("/api/singha/v1/schema/" + name + "/*.json", "classpath:config/apps/" + name + "/");
    }

    @Override
    public String toString() {
        return "AppDefinition{" + "name=" + name + ", urlPrefixProperty=" + urlPrefixProperty + ", schemas=" + schemas + ", mappings=" + mappings + '}';
    }
}
