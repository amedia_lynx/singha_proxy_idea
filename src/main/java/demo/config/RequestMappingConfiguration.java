package demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.io.Resources;
import demo.config.model.AppDefinition;
import demo.config.model.MappingDefinition;
import demo.controller.GenericController;
import demo.service.GaiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BinaryOperator;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 *
 * @author boonyasukd
 */
@Configuration
@ConfigurationProperties(prefix = "config")
public class RequestMappingConfiguration {
    private static final String APP_CONFIGS = "classpath*:config/apps/*/index.yml";
    private static final BinaryOperator DISALLOW_DUPS = (a, b) -> {
        throw new RuntimeException("No dups allowed!");
    };

    private final ObjectMapper ymlReader = new ObjectMapper(new YAMLFactory());

    @Autowired
    private RequestMappingHandlerMapping mappings;

    @Autowired
    private GenericController controller;

    @Autowired
    private GaiaService gaia;

    private String singhaUrlPrefix;
    private String singhaPathPrefix;
    private Map<String, AppDefinition> apps = new LinkedHashMap<>();

    @PostConstruct
    public void postConstruct() {
        try {
            Method requestMethod = getMethodByName(controller.getClass(), "genericRequestHandler");
            Method requestWithBodyMethod = getMethodByName(controller.getClass(), "genericRequestWithBodyHandler");

            apps = Arrays.stream(new PathMatchingResourcePatternResolver().getResources(APP_CONFIGS))
                    .map(this::toAppDefinition)
                    .peek(AppDefinition::initSchemaList)
                    .collect(toMap(AppDefinition::getName, identity(), DISALLOW_DUPS, LinkedHashMap::new));

            apps.values().stream().forEach(appDef -> {
                appDef.getMappings().stream().forEach(mapping -> {
                    String serviceUrlPrefix = gaia.getProperty(appDef.getUrlPrefixProperty());
                    mapping.setParent(appDef);
                    mapping.initUrlTemplates(singhaUrlPrefix, singhaPathPrefix, serviceUrlPrefix);
                    switch(mapping.getMethod()) {
                        case POST:
                        case PUT:
                        case PATCH:
                            mappings.registerMapping(mapping.asRequestMappingInfo(singhaPathPrefix + "/" + appDef.getName()), controller, requestWithBodyMethod);
                            break;
                        default:
                            mappings.registerMapping(mapping.asRequestMappingInfo(singhaPathPrefix + "/" + appDef.getName()), controller, requestMethod);
                    }
                    System.out.println(mapping);
                });
            });
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private AppDefinition toAppDefinition(Resource resource) {
        try {
            return ymlReader.readValue(Resources.toString(resource.getURL(), Charset.forName("UTF-8")), AppDefinition.class);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private Method getMethodByName(Class<?> clazz, String methodName) {
        return Arrays.stream(clazz.getDeclaredMethods())
                .filter(method -> method.getName().equals(methodName))
                .findFirst().orElseThrow(() -> new RuntimeException("cannot find the specified method"));
    }

    public String getSinghaUrlPrefix() {
        return singhaUrlPrefix;
    }

    public void setSinghaUrlPrefix(String singhaUrlPrefix) {
        this.singhaUrlPrefix = singhaUrlPrefix;
    }

    public String getSinghaPathPrefix() {
        return singhaPathPrefix;
    }

    public void setSinghaPathPrefix(String singhaPathPrefix) {
        this.singhaPathPrefix = singhaPathPrefix;
    }

    public Map<String, AppDefinition> getApps() {
        return apps;
    }

    public void setApps(Map<String, AppDefinition> apps) {
        this.apps = apps;
    }

    public MappingDefinition findMappingDefinition(HttpMethod method, String urlPattern) {
        RequestMethod reqMethod = RequestMethod.valueOf(method.name());

        return apps.values().stream()
                .flatMap(app -> app.getMappings().stream())
                .filter(mapping -> mapping.matches(reqMethod, urlPattern))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Mapping definition not found!"));
    }
}
