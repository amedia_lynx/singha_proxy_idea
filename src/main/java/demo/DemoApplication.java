package demo;

import demo.config.MvcConfiguration;
import demo.service.SinghaRestErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class DemoApplication {

    private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    @ConfigurationProperties(prefix = "config.restRequestFactory")
    public SimpleClientHttpRequestFactory requestFactory() {
        return new SimpleClientHttpRequestFactory();
    }

    @Bean
    public RestTemplate restTemplate(SimpleClientHttpRequestFactory factory) {
        RestTemplate result = new RestTemplate(factory);
        result.setErrorHandler(new SinghaRestErrorHandler());

        return result;
    }

    @Profile("default")
    @Bean
    public WebMvcConfigurerAdapter singhaMvcConfig(MvcConfiguration mvcConfig) {
        log.info("+++++++++++++++ creating run-time env MVC configuration");
        return mvcConfig.getMvcConfigurer();
    }
}
