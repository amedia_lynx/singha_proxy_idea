package demo.controller;

import com.fasterxml.jackson.databind.JsonNode;
import demo.service.RequestProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.stream.Collectors.toMap;
import static org.springframework.web.servlet.HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;

/**
 *
 * @author boonyasukd
 */
@RestController
public class GenericController {

    @Autowired
    private RequestProcessor processor;

    public JsonNode genericRequestHandler(HttpServletRequest request, HttpMethod method) {
        return processor.processRequest(getUrlPattern(request), method, collectAllVariables(request));
    }

    public JsonNode genericRequestWithBodyHandler(HttpServletRequest request, HttpMethod method, @RequestBody JsonNode body) {
        return processor.processRequest(getUrlPattern(request), method, collectAllVariables(request), body);
    }

    private Map<String, String> collectAllVariables(HttpServletRequest request) {
        Object map = request.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        Map<String, String> values = ((Map<String, Object>) map).entrySet().stream()
                .collect(toMap(Entry::getKey, entry -> entry.getValue().toString()));
        values.put("token", request.getParameter("token"));
        return values;
    }

    private String getUrlPattern(HttpServletRequest request) {
        return (String) request.getAttribute(BEST_MATCHING_PATTERN_ATTRIBUTE);
    }
}
