package demo.controller;

import com.fasterxml.jackson.databind.JsonNode;
import demo.config.RequestMappingConfiguration;
import demo.config.model.AppDefinition;
import demo.config.model.MappingDefinition;
import demo.service.proxy.JsonTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.DefaultUriTemplateHandler;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.springframework.http.HttpMethod.GET;

@RestController
public class MainController extends JsonTransformer {
    private static final BinaryOperator DISALLOW_DUPS = (a, b) -> {
        throw new RuntimeException("No dups allowed!");
    };

    @Autowired
    RequestMappingConfiguration mappingConfig;

    @RequestMapping("/api/singha/v1")
    public JsonNode showApplicationList(@RequestParam(value = "token", required = true) String token) throws Exception {
        List<JsonNode> applications = mappingConfig.getApps().values().stream().map(toAppNode(token)).collect(toList());
        return newObjectNode().putPOJO("applications", applications);
    }

    private Function<AppDefinition, JsonNode> toAppNode(String token) {
        return (appDef) -> {
            Map<String, Object> rootResourceLinks = appDef.getMappings().stream()
                    .filter(MappingDefinition::isRootNode)
                    .collect(toMap(MappingDefinition::getActionText, toRootResourceLink(token),
                            DISALLOW_DUPS, LinkedHashMap::new));
            List<JsonNode> schemaLinks = appDef.getSchemas().stream().map(toSchemaLink(appDef)).collect(toList());

            rootResourceLinks.put("schemas", schemaLinks);

            return newObjectNode()
                    .put("name", appDef.getName())
                    .putPOJO("_links", rootResourceLinks);
        };
    }

    private Function<MappingDefinition, JsonNode> toRootResourceLink(String token) {
        return (mapping) -> {
            String url = new DefaultUriTemplateHandler().expand(mapping.getInboundAbsoluteUrl(), token).toString();

            return newObjectNode()
                    .put("title", mapping.getActionText())
                    .put("href", url)
                    .put("method", GET.name());
        };
    }

    private Function<String, JsonNode> toSchemaLink(AppDefinition appDef) {
        return (fileName) -> {
            return newObjectNode()
                    .put("title", fileName)
                    .put("href", mappingConfig.getSinghaUrlPrefix() + "/schema/" + appDef.getName() + "/" + fileName + ".json")
                    .put("method", GET.name());
        };
    }
}
