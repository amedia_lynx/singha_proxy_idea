package demo;

import com.google.common.collect.ImmutableMap;
import java.util.Map;
import org.junit.Test;
import org.springframework.web.util.DefaultUriTemplateHandler;

/**
 *
 * @author b0nyb0y
 */
public class UriTemplateHandlerTest {
    
    @Test
    public void testComplexPathVar() {
        DefaultUriTemplateHandler handler = new DefaultUriTemplateHandler();
        handler.setBaseUrl("http://localhost:9084");

        Map<String, String> vars = ImmutableMap.of("pathPrefix", "/api/odigos/v1", "wwwDomain", "www.rb.no", "netmeetingId", "13", "questionId", "26");
        System.out.println(handler.expand("{pathPrefix}/{wwwDomain}/netmeetings/{netmeetingId}/questions/{questionId}", vars).toString());
    }

    @Test
    public void testComplexPathVar2() {
        DefaultUriTemplateHandler handler = new DefaultUriTemplateHandler();
        Map<String, String> vars = ImmutableMap.of("urlPrefix", "http://localhost:9084", "pathPrefix", "/api/odigos/v1", "wwwDomain", "www.rb.no", "netmeetingId", "13", "questionId", "26");
        System.out.println(handler.expand("{urlPrefix}{pathPrefix}/{wwwDomain}/netmeetings/{netmeetingId}/questions/{questionId}", vars).toString());
    }
}
